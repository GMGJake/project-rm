﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
[ExecuteInEditMode]
public class RaymarchCamera : SceneViewFilter
{
    public Shader shader;
    private Material raymarchMaterial;
    public Material RaymarchMaterial
    {
        get
        {
            if(raymarchMaterial == null && shader != null)
            {
                raymarchMaterial = new Material(shader)
                {
                    hideFlags = HideFlags.HideAndDontSave
                };
            }
            return raymarchMaterial;
        }
    }

    private new Camera camera;
    public Camera Camera
    {
        get { return camera ?? (camera = GetComponent<Camera>()); }
    }
    public float maxDistance;
    [Range(1, 300)]
    public int maxIterations = 128;
    [Range(0.1f, 0.001f)]
    public float accuracy = 0.1f;

    [Header("Directional Light")]
    public Light directionalLight;

    [Header("Colors")]
    public Color mainColor;
    public Color missColor;

    [Header("AO")]
    [Range(0.01f, 10f)]
    public float aoStepSize;
    [Range(1, 5)]
    public int aoIterations;
    [Range(0, 1)]
    public float aoIntensity;

    [Header("Shadows")]
    [Range(1, 128)]
    public float shadowPenumbra;
    [Range(0, 4)]
    public float shadowIntensity;
    public Vector2 shadowDistance = new Vector2(0.1f, 3f);

    [Header("Signed Distance Field")]
    public Vector3 modInterval;
    public Transform sphere;
    public Transform box;
    public float boxRounding;
    public float boxSphereSmoothing;
    public Transform sphere2;
    public float sphereIntersectSmoothing;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (RaymarchMaterial == null)
        {
            Graphics.Blit(source, destination);
            return;
        }

        RaymarchMaterial.SetColor("_mainColor", mainColor);
        RaymarchMaterial.SetColor("_missColor", missColor);
        RaymarchMaterial.SetVector("_LightDir", directionalLight ? directionalLight.transform.forward : Vector3.down);
        RaymarchMaterial.SetColor("_LightCol", directionalLight ? directionalLight.color : Color.black);
        RaymarchMaterial.SetFloat("_LightIntensity", directionalLight ? directionalLight.intensity : 1);
        RaymarchMaterial.SetFloat("_ShadowIntensity", shadowIntensity);
        RaymarchMaterial.SetFloat("_ShadowPenumbra", shadowPenumbra);
        RaymarchMaterial.SetInt("_MaxIterations", maxIterations);
        RaymarchMaterial.SetFloat("_Accuracy", accuracy);
        RaymarchMaterial.SetVector("_ShadowDistance", shadowDistance); 
        RaymarchMaterial.SetMatrix("_CamFrustum", CamFrustum(Camera));
        RaymarchMaterial.SetMatrix("_CamToWorld", Camera.cameraToWorldMatrix);
        RaymarchMaterial.SetFloat("_maxDistance", maxDistance);
        RaymarchMaterial.SetFloat("_box1Round", boxRounding);
        RaymarchMaterial.SetFloat("_boxSphereSmooth", boxSphereSmoothing);
        RaymarchMaterial.SetFloat("_sphereIntersectSmooth", sphereIntersectSmoothing);
        RaymarchMaterial.SetVector("_sphere1", TransformToVector4(sphere));
        RaymarchMaterial.SetVector("_sphere2", TransformToVector4(sphere2));
        RaymarchMaterial.SetVector("_box1", TransformToVector4(box));
        RaymarchMaterial.SetVector("_modInterval", modInterval);

        RaymarchMaterial.SetFloat("_AoStepSize", aoStepSize);
        RaymarchMaterial.SetInt("_AoIterations", aoIterations);
        RaymarchMaterial.SetFloat("_AoIntensity", aoIntensity);

        RenderTexture.active = destination;
        RaymarchMaterial.SetTexture("_MainTex", source);
        GL.PushMatrix();
        GL.LoadOrtho();
        RaymarchMaterial.SetPass(0);
        GL.Begin(GL.QUADS);

        //BL 
        GL.MultiTexCoord2(0, 0f, 0f);
        GL.Vertex3(0f, 0f, 3f); // third arg corresponds to the row index of BL set in CamFrustum method, and is retrieved by the shader's vert function
        //BR
        GL.MultiTexCoord2(0, 1f, 0f);
        GL.Vertex3(1f, 0f, 2f);
        //TR 
        GL.MultiTexCoord2(0, 1f, 1f);
        GL.Vertex3(1f, 1f, 1f);
        //TL 
        GL.MultiTexCoord2(0, 0f, 1f);
        GL.Vertex3(0f, 1f, 0f);

        GL.End();
        GL.PopMatrix();
    }

    private Vector4 TransformToVector4(Transform t)
    {
        if (t == null || !t.gameObject.activeSelf)
            return new Vector4();
        return new Vector4(t.position.x, t.position.y, t.position.z, t.localScale.x);
    }

    private Matrix4x4 CamFrustum(Camera cam)
    {
        Matrix4x4 frustum = Matrix4x4.identity;
        float fov = Mathf.Tan((cam.fieldOfView * 0.5f) * Mathf.Deg2Rad);

        Vector3 up = Vector3.up * fov;
        Vector3 right = Vector3.right * fov * cam.aspect;

        Vector3 TL = (-Vector3.forward - right + up);
        Vector3 TR = (-Vector3.forward + right + up);
        Vector3 BL = (-Vector3.forward - right - up);
        Vector3 BR = (-Vector3.forward + right - up);

        frustum.SetRow(0, TL);
        frustum.SetRow(1, TR);
        frustum.SetRow(2, BR);
        frustum.SetRow(3, BL);

        return frustum;
    }
}
