﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarchControls : MonoBehaviour
{
    public RaymarchCamera raymarchCamera;
    public float speed = 1;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 v = raymarchCamera.modInterval;

        float s = speed;
        bool boost = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        if (boost)
        {
            s *= 2;
        }

        if (Input.GetKey(KeyCode.I))
        {
            v.x += s * Time.deltaTime;
            v.y += s * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.J))
        {
            v.x -= s * Time.deltaTime;
            v.y -= s * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.O))
        {
            v.z += s * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.K))
        {
            v.z -= s * Time.deltaTime;
        }

        raymarchCamera.modInterval = v;
    }
}
