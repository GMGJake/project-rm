﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotate : MonoBehaviour
{
    public Vector3 vector;

    void Update()
    {
        transform.eulerAngles += vector * Time.deltaTime;
    }
}
