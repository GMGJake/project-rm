﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flyer : MonoBehaviour
{
    public float speed = 4;
    private bool hMoved = false;

    void Update()
    {
        float x = hMoved ? 0 : Input.GetAxisRaw("Horizontal") / 1;
        float y = Input.GetButtonDown("Jump") ? 1 : 0;
        float z = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        Vector3 m = new Vector3(x, y, z);
        transform.Translate(m);

        hMoved = Input.GetAxisRaw("Horizontal") != 0;

        if (Input.GetMouseButtonDown(0))
        {
            transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
        }
    }
}
